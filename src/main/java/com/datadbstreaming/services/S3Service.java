package com.datadbstreaming.services;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;


import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;


@Service
public class S3Service {

    @Autowired
    private AmazonS3 s3Client;

    @Value("${s3.bucket}")
    private String bucketName;
    // TODO: Add JAXB capabilities
    public void multiFileUpload(String virtualDestDir, String sourceDir, String[] fileNames) {
        ArrayList<File> files = new ArrayList<File>();
        for (String fileName : fileNames) {
            files.add(new File(sourceDir + "/" + fileName));
        }

        TransferManager tm = TransferManagerBuilder.standard()
                .withS3Client(s3Client)
                .build();;
        try {
            MultipleFileUpload multipleFileUpload = tm.uploadFileList(bucketName, virtualDestDir, new File(sourceDir), files);
            multipleFileUpload.waitForCompletion();
        }
        catch (AmazonServiceException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        tm.shutdownNow();
    }

    public void downloadFile(String key, String filePath) {
        TransferManager tm = TransferManagerBuilder.standard()
                .withS3Client(s3Client)
                .build();
        try {
            Download download = tm.download(bucketName, key, new File(filePath));
            System.out.println("Object download complete");
            download.waitForCompletion();

        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        tm.shutdownNow();
    }

    public void uploadFile(String keyName, String filePath) {
        TransferManager tm = TransferManagerBuilder.standard()
                .withS3Client(s3Client)
                .build();
        try {
            // TransferManager processes all transfers asynchronously,
            // so this call returns immediately.
            Upload upload = tm.upload(bucketName, keyName, new File(filePath));
            System.out.println("Object upload started");

            // Optionally, wait for the upload to finish before continuing.
            upload.waitForCompletion();
            System.out.println("Object upload complete");
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        tm.shutdownNow();

    }
}
