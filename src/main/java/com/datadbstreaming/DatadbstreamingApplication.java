package com.datadbstreaming;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Value;

import com.datadbstreaming.services.S3Service;

@SpringBootApplication
public class DatadbstreamingApplication implements CommandLineRunner {

	@Autowired
	S3Service s3Service;

	@Value("${s3.uploadFilePath}")
	private String uploadFilePath;

	@Value("${s3.downloadFilePath}")
	private String downloadFilePath;

	@Value("${s3.sourceDir}")
	private String sourceDir;

	public static void main(String[] args) {
		SpringApplication.run(DatadbstreamingApplication.class, args);
	}

	public void run(String... args) throws Exception {
		//s3Service.uploadFile("s3-file-upload.txt", uploadFilePath);
		//s3Service.downloadFile("s3-file-upload.txt", downloadFilePath);
		String[] files = new String[] {"file1", "file2", "file3", "file4", "file5"};
		//s3Service.multiFileUpload("multiUpload", sourceDir, files);
	}
}
